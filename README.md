## qssi-user 11 RP1A.200720.012 eng.compil.20211208.155903 release-keys
- Manufacturer: vivo
- Platform: 
- Codename: PD2056
- Brand: vivo
- Flavor: qssi-user
- Release Version: 11
- Id: RP1A.200720.012
- Incremental: eng.compil.20211208.155903
- Tags: release-keys
- CPU Abilist: arm64-v8a,armeabi-v7a,armeabi
- A/B Device: false
- Locale: zh-CN
- Screen Density: undefined
- Fingerprint: vivo/PD2056/PD2056:11/RP1A.200720.012/compiler1208155903:user/release-keys
- OTA version: 
- Branch: qssi-user-11-RP1A.200720.012-eng.compil.20211208.155903-release-keys
- Repo: vivo_pd2056_dump


>Dumped by [Phoenix Firmware Dumper](https://github.com/DroidDumps/phoenix_firmware_dumper)
