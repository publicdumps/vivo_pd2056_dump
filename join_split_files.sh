#!/bin/bash

cat system/system/priv-app/SystemUI/SystemUI.apk.* 2>/dev/null >> system/system/priv-app/SystemUI/SystemUI.apk
rm -f system/system/priv-app/SystemUI/SystemUI.apk.* 2>/dev/null
cat system/system/priv-app/Settings/Settings.apk.* 2>/dev/null >> system/system/priv-app/Settings/Settings.apk
rm -f system/system/priv-app/Settings/Settings.apk.* 2>/dev/null
cat system/system/priv-app/Baidu/Baidu.apk.* 2>/dev/null >> system/system/priv-app/Baidu/Baidu.apk
rm -f system/system/priv-app/Baidu/Baidu.apk.* 2>/dev/null
cat system/system/product/app/WebViewGoogle/WebViewGoogle.apk.* 2>/dev/null >> system/system/product/app/WebViewGoogle/WebViewGoogle.apk
rm -f system/system/product/app/WebViewGoogle/WebViewGoogle.apk.* 2>/dev/null
cat system/system/custom/app/WavesBase/WavesBase.apk.* 2>/dev/null >> system/system/custom/app/WavesBase/WavesBase.apk
rm -f system/system/custom/app/WavesBase/WavesBase.apk.* 2>/dev/null
cat system/system/app/VivoThemeRes/VivoThemeRes.apk.* 2>/dev/null >> system/system/app/VivoThemeRes/VivoThemeRes.apk
rm -f system/system/app/VivoThemeRes/VivoThemeRes.apk.* 2>/dev/null
cat system/system/apex/com.android.art.release.apex.* 2>/dev/null >> system/system/apex/com.android.art.release.apex
rm -f system/system/apex/com.android.art.release.apex.* 2>/dev/null
cat system/system/apex/com.android.vndk.current.apex.* 2>/dev/null >> system/system/apex/com.android.vndk.current.apex
rm -f system/system/apex/com.android.vndk.current.apex.* 2>/dev/null
